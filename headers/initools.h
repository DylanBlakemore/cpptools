/*
 * initools.h
 *
 *  Created on: 10 Jul 2017
 *      Author: dylan
 */

#ifndef INITOOLS_H_
#define INITOOLS_H_
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <boost/algorithm/string.hpp>

class IniFile
{
public:
	IniFile()
	{
		mode='n';
		fpath="";
		delim=':';
	}
	IniFile(std::string path, char delimiter=':')
	{
		fpath=path;
		delim=delimiter;
		mode='n';
	}
	int open(char m);
	void close();
	std::string readString(std::string header, std::string key);
	bool readBool(std::string header, std::string key);
	float readFloat(std::string header, std::string key);
	int readInt(std::string header, std::string key);
private:
	char mode, delim;
	std::string fpath;
	std::ofstream outfile;
	std::ifstream infile;
};



#endif /* INITOOLS_H_ */
