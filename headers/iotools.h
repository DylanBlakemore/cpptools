/*
 * iotools.h
 *
 *  Created on: 10 Jul 2017
 *      Author: dylan
 */

#ifndef IOTOOLS_H_
#define IOTOOLS_H_
#include <string>

typedef int (*func_ptr)(std::string);

std::string getInput(std::string question)
{
	std::string input;
	std::cout << question << std::endl;
	std::cin >> input;
	return input;
}

std::string getVerifiedInput(std::string question, func_ptr func_in)
{
	std::string input;
	int valid = 0;
	do {
		input = getInput(question);
		if(func_in(input) == 0) {
			std::cout << "Invalid input, please try again: " << std::endl;
		} else {
			valid = 1;
		}
	}while(!valid);
	return input;
}

void coutLine(std::string line)
{
	std::cout << line << std::endl;
}

void coutPhrase(std::string in)
{
	std:: cout << in;
}



#endif /* IOTOOLS_H_ */
