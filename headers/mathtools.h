/*
 * normalize.h
 *
 *  Created on: 10 Jul 2017
 *      Author: dylan
 */

#ifndef MATHTOOLS_H_
#define MATHTOOLS_H_

#include <vector>
#include <algorithm>
#include <math.h>

std::vector<float> linearNorm(std::vector<float>& in, float min, float max)
{
	std::vector<float> normal(in.size());
	for(unsigned int i=0; i<in.size(); i++) {
		float norm = (in[i] - min)/(max - min);
		if(norm < 0)norm=0;
		if(norm > 1)norm=1;
		normal[i] = norm;
	}
	return normal;
}

std::vector<float> linearNorm(std::vector<float>& in)
{
	std::vector<float>::iterator max_it;
	std::vector<float>::iterator min_it;

	max_it = std::max_element(in.begin(), in.end());
	min_it = std::min_element(in.begin(), in.end());
	return linearNorm(in, *min_it, *max_it);
}

std::vector<float> logNorm(std::vector<float>& in, float min, float max)
{
	std::vector<float> logv(in.size());
	std::vector<float>::iterator min_it = std::min_element(in.begin(), in.end());

	for(unsigned int i=0; i<in.size(); i++) {
		float tmp = in[i] - *min_it + 1;
		if(in[i] < min)tmp = min - *min_it + 1;
		if(in[i] > max)tmp = max - *min_it + 1;
		logv[i] = log(tmp);
	}
	return linearNorm(logv);
}

std::vector<float> logNorm(std::vector<float>& in)
{
	std::vector<float>::iterator max_it;
	std::vector<float>::iterator min_it;

	max_it = std::max_element(in.begin(), in.end());
	min_it = std::min_element(in.begin(), in.end());
	return logNorm(in, *min_it, *max_it);
}





#endif /* MATHTOOLS_H_ */
