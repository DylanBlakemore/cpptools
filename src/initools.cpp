/*
 * initools.cpp
 *
 *  Created on: 10 Jul 2017
 *      Author: dylan
 */

#include "../headers/initools.h"
#include <string.h>

std::string IniFile::readString(std::string header, std::string key)
{
	if(mode == 'w') return "Wrong mode";
	if(mode == 'n') return "Not open";

	std::string fullheader = std::string("[") + header + std::string("]");
	std::string value = "";
	std::string line;
	std::string items[2];
	while(infile.good()) {
		// loop until the header is found
		getline(infile, line);
		boost::trim(line);
		if(line.empty())continue;

		if(line.compare(fullheader)==0)
		{
			while(infile.good()) {
				// loop until the key is found. Break if it is not found
				getline(infile, line);
				boost::trim(line);
				if(line.empty())continue;
				if(line[0] == '[')break;

				std::stringstream stream(line);
				getline(stream, items[0], delim);
				getline(stream, items[1], delim);
				if(items[0].compare(key)==0) {
					value = items[1];
					break;
				}
				else
					continue;
			}
			break;
		}
	}
	return value;
}

float IniFile::readFloat(std::string header, std::string key)
{
	return strtof(readString(header, key).c_str(),0);
}

int IniFile::readInt(std::string header, std::string key)
{
	return atoi(readString(header, key).c_str());
}

bool IniFile::readBool(std::string header, std::string key)
{
	std::string item = readString(header, key);
	boost::algorithm::to_lower(item);
	if(item.compare("false"))
		return 0;
	else if(item.compare("true"))
		return 1;
	else
		return 0;
}

int IniFile::open(char m)
{
	if(mode != 'n') {
		return 0;
	}
	if(m == 'w') {
		mode = m;
		outfile.open(fpath.c_str());
		return 1;
	}
	if(m == 'r') {
		mode = m;
		infile.open(fpath.c_str());
		return 1;
	}
	return 0;
}

void IniFile::close()
{
	if(mode == 'w')outfile.close();
	if(mode == 'r')infile.close();
}
